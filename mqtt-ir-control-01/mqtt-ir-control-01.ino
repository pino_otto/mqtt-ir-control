/**
 * IotWebConf06MqttApp.ino -- IotWebConf is an ESP8266/ESP32
 *   non blocking WiFi/AP web configuration library for Arduino.
 *   https://github.com/prampec/IotWebConf 
 *
 * Copyright (C) 2018 Balazs Kelemen <prampec+arduino@gmail.com>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

/**
 * Example: MQTT Demo Application
 * Description:
 *   All IotWebConf specific aspects of this example are described in
 *   previous examples, so please get familiar with IotWebConf before
 *   starting this example. So nothing new will be explained here, 
 *   but a complete demo application will be build.
 *   It is also expected from the reader to have a basic knowledge over
 *   MQTT to understand this code.
 *   
 *   This example starts an MQTT client with the configured
 *   connection settings.
 *   Will post the status changes of the D2 pin in channel "/test/status".
 *   Receives messages appears in channel "/test/action", and writes them to serial.
 *   This example also provides the firmware update option.
 *   (See previous examples for more details!)
 * 
 * Software setup for this example:
 *   This example utilizes Joel Gaehwiler's MQTT library.
 *   https://github.com/256dpi/arduino-mqtt
 * 
 * Hardware setup for this example (receive IR):
 *   - An LED is attached to LED_BUILTIN pin with setup On=LOW.
 *   - [Optional] A push button is attached to pin D2, the other leg of the
 *     button should be attached to GND.
 *
 * An IR LED circuit *MUST* be connected to the ESP8266 on a pin
 * as specified by kIrLed below.
 *
 * TL;DR: The IR LED needs to be driven by a transistor for a good result.
 *
 * Suggested circuit:
 *     https://github.com/markszabo/IRremoteESP8266/wiki#ir-sending
 *
 * Common mistakes & tips:
 *   * Don't just connect the IR LED directly to the pin, it won't
 *     have enough current to drive the IR LED effectively.
 *   * Make sure you have the IR LED polarity correct.
 *     See: https://learn.sparkfun.com/tutorials/polarity/diode-and-led-polarity
 *   * Typical digital camera/phones can be used to see if the IR LED is flashed.
 *     Replace the IR LED with a normal LED if you don't have a digital camera
 *     when debugging.
 *   * Avoid using the following pins unless you really know what you are doing:
 *     * Pin 0/D3: Can interfere with the boot/program mode & support circuits.
 *     * Pin 1/TX/TXD0: Any serial transmissions from the ESP8266 will interfere.
 *     * Pin 3/RX/RXD0: Any serial transmissions to the ESP8266 will interfere.
 *   * ESP-01 modules are tricky. We suggest you use a module with more GPIOs
 *     for your first time. e.g. ESP-12 etc. 
 */

#include <MQTT.h>
#include <IotWebConf.h>
#include <IRremoteESP8266.h>
#include <IRutils.h>
#include <IRrecv.h>
#include <IRsend.h>
#include <EEPROM.h>

#define DECODE_AC 1


// ==================== start of TUNEABLE PARAMETERS for IR Dump ====================
// An IR detector/demodulator is connected to GPIO pin 14
// e.g. D5 on a NodeMCU board.
const uint16_t kRecvPin = 14;

// The Serial connection baud rate.
// i.e. Status message will be sent to the PC at this baud rate.
// Try to avoid slow speeds like 9600, as you will miss messages and
// cause other problems. 115200 (or faster) is recommended.
// NOTE: Make sure you set your Serial Monitor to the same speed.
const uint32_t kBaudRate = 115200;

// As this program is a special purpose capture/decoder, let us use a larger
// than normal buffer so we can handle Air Conditioner remote codes.
const uint16_t kCaptureBufferSize = 1024;

// kTimeout is the Nr. of milli-Seconds of no-more-data before we consider a
// message ended.
// This parameter is an interesting trade-off. The longer the timeout, the more
// complex a message it can capture. e.g. Some device protocols will send
// multiple message packets in quick succession, like Air Conditioner remotes.
// Air Coniditioner protocols often have a considerable gap (20-40+ms) between
// packets.
// The downside of a large timeout value is a lot of less complex protocols
// send multiple messages when the remote's button is held down. The gap between
// them is often also around 20+ms. This can result in the raw data be 2-3+
// times larger than needed as it has captured 2-3+ messages in a single
// capture. Setting a low timeout value can resolve this.
// So, choosing the best kTimeout value for your use particular case is
// quite nuanced. Good luck and happy hunting.
// NOTE: Don't exceed kMaxTimeoutMs. Typically 130ms.
#if DECODE_AC
// Some A/C units have gaps in their protocols of ~40ms. e.g. Kelvinator
// A value this large may swallow repeats of some protocols
const uint8_t kTimeout = 50;
#else   // DECODE_AC
// Suits most messages, while not swallowing many repeats.
const uint8_t kTimeout = 15;
#endif  // DECODE_AC
// Alternatives:
// const uint8_t kTimeout = 90;
// Suits messages with big gaps like XMP-1 & some aircon units, but can
// accidentally swallow repeated messages in the rawData[] output.
//
// const uint8_t kTimeout = kMaxTimeoutMs;
// This will set it to our currently allowed maximum.
// Values this high are problematic because it is roughly the typical boundary
// where most messages repeat.
// e.g. It will stop decoding a message and start sending it to serial at
//      precisely the time when the next message is likely to be transmitted,
//      and may miss it.

// Set the smallest sized "UNKNOWN" message packets we actually care about.
// This value helps reduce the false-positive detection rate of IR background
// noise as real messages. The chances of background IR noise getting detected
// as a message increases with the length of the kTimeout value. (See above)
// The downside of setting this message too large is you can miss some valid
// short messages for protocols that this library doesn't yet decode.
//
// Set higher if you get lots of random short UNKNOWN messages when nothing
// should be sending a message.
// Set lower if you are sure your setup is working, but it doesn't see messages
// from your device. (e.g. Other IR remotes work.)
// NOTE: Set this value very high to effectively turn off UNKNOWN detection.
const uint16_t kMinUnknownSize = 12;
// ==================== end of TUNEABLE PARAMETERS for IR Dump ====================

// Use turn on the save buffer feature for more complete capture coverage.
IRrecv irrecv(kRecvPin, kCaptureBufferSize, kTimeout, true);
decode_results results;  // Somewhere to store the results of IR dump

const uint16_t kIrLed = 4;  // ESP8266 GPIO pin to use. Recommended: 4 (D2).
IRsend irsend(kIrLed);      // Set the GPIO to be used to sending the message.

// -- Initial name of the Thing. Used e.g. as SSID of the own Access Point.
const char thingName[] = "myDevice";

// -- Initial password to connect to the Thing, when it creates an own Access Point.
const char wifiInitialApPassword[] = "smrtTHNG8266";

#define STRING_LEN 128

// -- Configuration specific key. The value should be modified if config structure was changed.
#define CONFIG_VERSION "mqt1"

// -- When CONFIG_PIN is pulled to ground on startup, the Thing will use the initial
//      password to buld an AP. (E.g. in case of lost password)
#define CONFIG_PIN D1

// -- Status indicator pin.
//      First it will light up (kept LOW), on Wifi connection it will blink,
//      when connected to the Wifi it will turn off (kept HIGH).
#define STATUS_PIN LED_BUILTIN

#define IR_LEARN_COMMAND_PIN  D3 // D3 pin has pullup
#define ON_LED_PIN            D6
#define OFF_LED_PIN           D7  

// MQTT
#define COMMAND_TOPIC     "test/action"
#define COMMAND_ON        "ON"
#define COMMAND_OFF       "OFF"


// -- Callback method declarations.
void wifiConnected();
void configSaved();
boolean formValidator();
void mqttMessageReceived(String &topic, String &payload);

DNSServer dnsServer;
WebServer server(80);
HTTPUpdateServer httpUpdater;
WiFiClient net;
MQTTClient mqttClient;

char mqttServerValue[STRING_LEN];
char mqttUserNameValue[STRING_LEN];
char mqttUserPasswordValue[STRING_LEN];

IotWebConf iotWebConf(thingName, &dnsServer, &server, wifiInitialApPassword, CONFIG_VERSION);
IotWebConfParameter mqttServerParam = IotWebConfParameter("MQTT server", "mqttServer", mqttServerValue, STRING_LEN);
IotWebConfParameter mqttUserNameParam = IotWebConfParameter("MQTT user", "mqttUser", mqttUserNameValue, STRING_LEN);
IotWebConfParameter mqttUserPasswordParam = IotWebConfParameter("MQTT password", "mqttPass", mqttUserPasswordValue, STRING_LEN, "password");

boolean needMqttConnect = false;
boolean needReset = false;
int pinState = HIGH;
unsigned long lastReport = 0;
unsigned long lastMqttConnectionAttempt = 0;

//EEPROM struct
struct { 
    uint16_t on_length = 0;
    uint16_t on_rawData[500];
    uint16_t off_length = 0;
    uint16_t off_rawData[500];
} ir_data;

int ir_data_address = 1024;

void setup() {
  Serial.begin(115200);
  while (!Serial)  // Wait for the serial connection to be establised.
    delay(50);
  Serial.println();
  Serial.println("Starting up...");

  pinMode(IR_LEARN_COMMAND_PIN, INPUT_PULLUP);
  pinMode(ON_LED_PIN, OUTPUT);
  pinMode(OFF_LED_PIN, OUTPUT);

  // turn off the LEDs
  digitalWrite(ON_LED_PIN, LOW);
  digitalWrite(OFF_LED_PIN, LOW);

  iotWebConf.setStatusPin(STATUS_PIN);
  iotWebConf.setConfigPin(CONFIG_PIN);
  iotWebConf.addParameter(&mqttServerParam);
  iotWebConf.addParameter(&mqttUserNameParam);
  iotWebConf.addParameter(&mqttUserPasswordParam);
  iotWebConf.setConfigSavedCallback(&configSaved);
  iotWebConf.setFormValidator(&formValidator);
  iotWebConf.setWifiConnectionCallback(&wifiConnected);
  iotWebConf.setupUpdateServer(&httpUpdater);

  // -- Initializing the configuration.
  boolean validConfig = iotWebConf.init();
  if (!validConfig) {
    mqttServerValue[0] = '\0';
    mqttUserNameValue[0] = '\0';
    mqttUserPasswordValue[0] = '\0';
  }

  // -- Set up required URL handlers on the web server.
  server.on("/", handleRoot);
  server.on("/config", []{ iotWebConf.handleConfig(); });
  server.onNotFound([](){ iotWebConf.handleNotFound(); });

  // start MQTT
  mqttClient.begin(mqttServerValue, net);
  mqttClient.onMessage(mqttMessageReceived);

  // start IR receiver (dump)
  #if DECODE_HASH
    // Ignore messages with less than minimum on or off pulses.
    irrecv.setUnknownThreshold(kMinUnknownSize);
  #endif                  // DECODE_HASH
  irrecv.enableIRIn();  // Start the IR receiver
  irsend.begin();       // Start the IR sender

  // set the EEPROM size
//  EEPROM.begin(4096);
  
  Serial.println("Ready.");
}

void loop() {
  // -- doLoop should be called as frequently as possible.
  iotWebConf.doLoop();
  mqttClient.loop();
  
  if (needMqttConnect) {
    if (connectMqtt()) {
      needMqttConnect = false;
    }
  } else if ((iotWebConf.getState() == IOTWEBCONF_STATE_ONLINE) && (!mqttClient.connected())) {
    Serial.println("MQTT reconnect");
    connectMqtt();
  }

  if (needReset) {
    Serial.println("Rebooting after 1 second.");
    iotWebConf.delay(1000);
    ESP.restart();
  }

  /////
  // check the learn push button
  int learnValue = digitalRead(IR_LEARN_COMMAND_PIN);
  Serial.print("first IR_LEARN_COMMAND_PIN = ");
  Serial.println(learnValue);
  if (learnValue == LOW) {
    delay(2000);
    learnValue = digitalRead(IR_LEARN_COMMAND_PIN);
    Serial.print("second IR_LEARN_COMMAND_PIN = ");
    Serial.println(learnValue);
    if (learnValue == HIGH) {
      digitalWrite(ON_LED_PIN, HIGH);
      /*
       * read the IR signal for ON
       */
      Serial.print("IRrecvDump is now running and waiting for IR input of ON signal on Pin ");
      Serial.println(kRecvPin);
      // Check if the IR code has been received
      if (irrecv.decode(&results)) {
        // Display a crude timestamp.
        uint32_t now = millis();
        Serial.printf("Timestamp : %06u.%03u\n", now / 1000, now % 1000);
        if (results.overflow)
          Serial.printf(
              "WARNING: IR code is too big for buffer (>= %d). "
              "This result shouldn't be trusted until this is resolved. "
              "Edit & increase kCaptureBufferSize.\n",
              kCaptureBufferSize);
        // Display the basic output of what we found.
        Serial.print(resultToHumanReadableBasic(&results));
        yield();  // Feed the WDT as the text output can take a while to print.
    
        // Display the library version the message was captured with.
        Serial.print("Library   : v");
        Serial.println(_IRREMOTEESP8266_VERSION_);
        Serial.println();
    
        // Output RAW timing info of the result.
        Serial.println(resultToTimingInfo(&results));
        yield();  // Feed the WDT (again)
    
        // Output the results as source code
        Serial.println(resultToSourceCode(&results));
        Serial.println("");  // Blank line between entries
        yield();             // Feed the WDT (again)

        // Output the results as hex
        Serial.println(resultToHexidecimal(&results));
        Serial.println("");  // Blank line between entries
        yield();             // Feed the WDT (again)

        String rawDataSourceCode = resultToSourceCode(&results);
        
        // get the size of raw data
        String sizeAsString = rawDataSourceCode.substring(rawDataSourceCode.indexOf('[') + 1, rawDataSourceCode.indexOf(']'));
        Serial.println("sizeAsString = " + sizeAsString);
//        Serial.println("");
        int rawDataSize = sizeAsString.toInt();
        Serial.print("rawDataSize = ");
        Serial.println(rawDataSize);

        // get the raw data
        String dataAsString = rawDataSourceCode.substring(rawDataSourceCode.indexOf('{') + 1, rawDataSourceCode.indexOf('}'));
        Serial.println("dataAsString = " + dataAsString);
        dataAsString.replace(" ", "");
        Serial.println("dataAsString without spaces = " + dataAsString);
        uint16_t dataArray[rawDataSize];
        getArray(dataAsString, dataArray, rawDataSize);
        Serial.println("dataArray = ");
        for (int i = 0; i < rawDataSize; i++) {
          Serial.print(dataArray[i]);
          Serial.print(",");
        }
        Serial.println("");  // Blank line between entries
        yield();             // Feed the WDT (again)
        
        //TODO call parseStringAndSendRaw(IRsend *irsend, const String str) from IRMQTTServer

        EEPROM.begin(4096);
        EEPROM.get(ir_data_address, ir_data);
        Serial.println("Old  ON values are: " + String(ir_data.on_length) + "," + String(ir_data.on_rawData[0]));
        Serial.println("Old OFF values are: " + String(ir_data.off_length) + "," + String(ir_data.off_rawData[0]));
        
//        // set the EEPROM data (example)
//        uint16_t src[5] = {1, 2, 3, 4, 5};
//        ir_data.on_length = 5;
//        memcpy(ir_data.on_rawData, src, sizeof(uint16_t)*5);

        // set the EEPROM data
        ir_data.on_length = rawDataSize;
        memcpy(ir_data.on_rawData, dataArray, sizeof(uint16_t)*rawDataSize);

        EEPROM.put(ir_data_address, ir_data);
        EEPROM.commit(); // disabled during development

        // verify data (testing)
        EEPROM.get(ir_data_address, ir_data);
        Serial.println("New  ON values are: " + String(ir_data.on_length) + "," + String(ir_data.on_rawData[0]));
        Serial.println("New OFF values are: " + String(ir_data.off_length) + "," + String(ir_data.off_rawData[0]));
        
        delay(1000); // keep the led ON for a while
        Serial.println("Read and stored the IR ON signal");
      }
      digitalWrite(ON_LED_PIN, LOW);
    } else { // button is still pushed down
      digitalWrite(OFF_LED_PIN, HIGH);
      /**
       * read the IR signal for OFF
       */
      Serial.print("IRrecvDump is now running and waiting for IR input of OFF signal on Pin ");
      Serial.println(kRecvPin);
      // wait unitl the IR code has been received
      while (!irrecv.decode(&results)) {
        delay(100);
      }
      // Display a crude timestamp.
      uint32_t now = millis();
      Serial.printf("Timestamp : %06u.%03u\n", now / 1000, now % 1000);
      if (results.overflow)
        Serial.printf(
            "WARNING: IR code is too big for buffer (>= %d). "
            "This result shouldn't be trusted until this is resolved. "
            "Edit & increase kCaptureBufferSize.\n",
            kCaptureBufferSize);
      // Display the basic output of what we found.
      Serial.print(resultToHumanReadableBasic(&results));
      yield();  // Feed the WDT as the text output can take a while to print.
  
      // Display the library version the message was captured with.
      Serial.print("Library   : v");
      Serial.println(_IRREMOTEESP8266_VERSION_);
      Serial.println();
  
      // Output RAW timing info of the result.
      Serial.println(resultToTimingInfo(&results));
      yield();  // Feed the WDT (again)
  
      // Output the results as source code
      Serial.println(resultToSourceCode(&results));
      Serial.println("");  // Blank line between entries
      yield();             // Feed the WDT (again)

      // Output the results as hex
      Serial.println(resultToHexidecimal(&results));
      Serial.println("");  // Blank line between entries
      yield();             // Feed the WDT (again)

      String rawDataSourceCode = resultToSourceCode(&results);
      
      // get the size of raw data
      String sizeAsString = rawDataSourceCode.substring(rawDataSourceCode.indexOf('[') + 1, rawDataSourceCode.indexOf(']'));
      Serial.println("sizeAsString = " + sizeAsString);
//        Serial.println("");
      int rawDataSize = sizeAsString.toInt();
      Serial.print("rawDataSize = ");
      Serial.println(rawDataSize);

      // get the raw data
      String dataAsString = rawDataSourceCode.substring(rawDataSourceCode.indexOf('{') + 1, rawDataSourceCode.indexOf('}'));
      Serial.println("dataAsString = " + dataAsString);
      dataAsString.replace(" ", "");
      Serial.println("dataAsString without spaces = " + dataAsString);
      uint16_t dataArray[rawDataSize];
      getArray(dataAsString, dataArray, rawDataSize);
      Serial.println("dataArray = ");
      for (int i = 0; i < rawDataSize; i++) {
        Serial.print(dataArray[i]);
        Serial.print(",");
      }
      Serial.println("");  // Blank line between entries
      yield();             // Feed the WDT (again)
      
      //TODO call parseStringAndSendRaw(IRsend *irsend, const String str) from IRMQTTServer

      EEPROM.begin(4096);
      EEPROM.get(ir_data_address, ir_data);
      Serial.println("Old  ON values are: " + String(ir_data.on_length) + "," + String(ir_data.on_rawData[0]));
      Serial.println("Old OFF values are: " + String(ir_data.off_length) + "," + String(ir_data.off_rawData[0]));
      
//        // set the EEPROM data (example)
//        uint16_t src[5] = {1, 2, 3, 4, 5};
//        ir_data.on_length = 5;
//        memcpy(ir_data.on_rawData, src, sizeof(uint16_t)*5);

      // set the EEPROM data
      ir_data.off_length = rawDataSize;
      memcpy(ir_data.off_rawData, dataArray, sizeof(uint16_t)*rawDataSize);

      EEPROM.put(ir_data_address, ir_data);
      EEPROM.commit(); // disabled during development

      // verify data (testing)
      EEPROM.get(ir_data_address, ir_data);
      Serial.println("New  ON values are: " + String(ir_data.on_length) + "," + String(ir_data.on_rawData[0]));
      Serial.println("New OFF values are: " + String(ir_data.off_length) + "," + String(ir_data.off_rawData[0]));
      
      delay(1000); // keep the led ON for a while
      Serial.println("Read and stored the IR OFF signal");
      digitalWrite(OFF_LED_PIN, LOW);
    }
  }

  delay(100); // to remove it

//  unsigned long now = millis();
//  if ((500 < now - lastReport) && (pinState != digitalRead(CONFIG_PIN))) {
//    pinState = 1 - pinState; // invert pin state as it is changed
//    lastReport = now;
//    Serial.print("Sending on MQTT channel '/test/status' :");
//    Serial.println(pinState == LOW ? "ON" : "OFF");
//    mqttClient.publish("/test/status", pinState == LOW ? "ON" : "OFF");
//  }
}

/**
 * Handle web requests to "/" path.
 */
void handleRoot() {
  // -- Let IotWebConf test and handle captive portal requests.
  if (iotWebConf.handleCaptivePortal()) {
    // -- Captive portal request were already served.
    return;
  }
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
  s += "<title>IotWebConf 06 MQTT App</title></head><body>MQTT App demo";
  s += "<ul>";
  s += "<li>MQTT server: ";
  s += mqttServerValue;
  s += "</ul>";
  s += "Go to <a href='config'>configure page</a> to change values.";
  s += "</body></html>\n";

  server.send(200, "text/html", s);
}

void wifiConnected() {
  needMqttConnect = true;
}

void configSaved() {
  Serial.println("Configuration was updated.");
  needReset = true;
}

boolean formValidator() {
  Serial.println("Validating form.");
  boolean valid = true;
  int l = server.arg(mqttServerParam.getId()).length();
  if (l < 3) {
    mqttServerParam.errorMessage = "Please provide at least 3 characters!";
    valid = false;
  }
  return valid;
}

boolean connectMqtt() {
  unsigned long now = millis();
  if (1000 > now - lastMqttConnectionAttempt) {
    // Do not repeat within 1 sec.
    return false;
  }
  Serial.println("Connecting to MQTT server...");
  if (!connectMqttOptions()) {
    lastMqttConnectionAttempt = now;
    return false;
  }
  Serial.println("Connected!");

  mqttClient.subscribe(COMMAND_TOPIC);
  return true;
}

/*
// -- This is an alternative MQTT connection method.
boolean connectMqtt() {
  Serial.println("Connecting to MQTT server...");
  while (!connectMqttOptions()) {
    iotWebConf.delay(1000);
  }
  Serial.println("Connected!");

  mqttClient.subscribe("/test/action");
  return true;
}
*/

boolean connectMqttOptions() {
  boolean result;
  if (mqttUserPasswordValue[0] != '\0') {
    result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue, mqttUserPasswordValue);
  } else if (mqttUserNameValue[0] != '\0') {
    result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue);
  } else {
    result = mqttClient.connect(iotWebConf.getThingName());
  }
  return result;
}

void mqttMessageReceived(String &topic, String &payload) {
  Serial.println("Incoming: " + topic + " - " + payload);
  if (topic == COMMAND_TOPIC) {
    if (payload == COMMAND_ON) {
      Serial.println("Received ON command");
      // get ON IR data from EEPROM
      EEPROM.begin(4096);
      EEPROM.get(ir_data_address, ir_data);
      Serial.println(" ON values are: " + String(ir_data.on_length) + "," + String(ir_data.on_rawData[0]));
      Serial.println("OFF values are: " + String(ir_data.off_length) + "," + String(ir_data.off_rawData[0]));
      uint16_t rawDataSize = ir_data.on_length;
      uint16_t dataArray[rawDataSize];
      memcpy(dataArray, ir_data.on_rawData, sizeof(uint16_t)*rawDataSize);
      // send IR data
      irsend.sendRaw(dataArray, rawDataSize, 38);  // Send a raw data capture at 38kHz
      irrecv.resume(); // TODO to test it
    } else if (payload == COMMAND_OFF) {
      Serial.println("Received OFF command");
      // get OFF IR data from EEPROM
      EEPROM.begin(4096);
      EEPROM.get(ir_data_address, ir_data);
      Serial.println(" ON values are: " + String(ir_data.on_length) + "," + String(ir_data.on_rawData[0]));
      Serial.println("OFF values are: " + String(ir_data.off_length) + "," + String(ir_data.off_rawData[0]));
      uint16_t rawDataSize = ir_data.off_length;
      uint16_t dataArray[rawDataSize];
      memcpy(dataArray, ir_data.off_rawData, sizeof(uint16_t)*rawDataSize);
      // send IR data
      irsend.sendRaw(dataArray, rawDataSize, 38);  // Send a raw data capture at 38kHz
      irrecv.resume(); // TODO to test it
    }
  }
  
}

void getArray(String inString, uint16_t *outArray, int size) {
  uint16_t r=0, t=0;
  //String oneLine = "123,456,789,999,";
  inString.concat(",");
  
  for (int i=0; i < inString.length(); i++) { 
    if (inString.charAt(i) == ',') { 
      outArray[t] = inString.substring(r, i).toInt(); 
      r=(i+1); 
      t++; 
    }
  }
}

std::vector<String> splitStringToVector(String msg){
  std::vector<String> subStrings;
  int j=0;
  for(int i =0; i < msg.length(); i++){
    if(msg.charAt(i) == ','){
      subStrings.push_back(msg.substring(j,i));
      j = i+1;
    }
  }
  subStrings.push_back(msg.substring(j,msg.length())); //to grab the last value of the string
  return subStrings;
}
