Programmable infrared (IR) remote controller accessible from internet via MQTT.

It has the following functionalites:
 - WIFI and MQTT configuration easily done via web browser on mobile phone
 - copy (dump) any commands from existing IR remote controllers
 - store the dumped IR signals to the EEPROM
 - receive commands via MQTT
 - send the dumped IR signals (e.g. ON/OFF to an air conditioner)